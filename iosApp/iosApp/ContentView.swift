import SwiftUI
import shared


struct ScooterRow: View {
    var name: String
    
    var body: some View {
        Text("Scooter: \(name)")
    }
}

struct ContentView: View {
	let greeting = GreetingHelper()
    @State var greet = "Loading..."
    

	var body: some View {
        VStack {
            Text(greet)
        }.onAppear {
            greeting.greet { result, error in
                self.greet = result?.first?.title ?? ""
            }
        }
	}
}

struct ContentView_Previews: PreviewProvider {
	static var previews: some View {
		ContentView()
	}
}
