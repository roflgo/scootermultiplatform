import SwiftUI
import shared

@main
struct iOSApp: App {
    
    init() {
        HelperKt.doInitKoinIos()
    }
    
	var body: some Scene {
		WindowGroup {
			ContentView()
		}
	}
}
