package com.example.multiplatofrm.electroscooterapplication.android

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.lifecycle.lifecycleScope
import co.touchlab.kampkit.ktor.ScooterApiImpl
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity() {
    private val presenter: ScooterApiImpl by inject()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val tv: TextView = findViewById(R.id.error_text)
        tv.text = "Test"
        lifecycleScope.launch {
            kotlin.runCatching {
                presenter.getJsonFromApi()
            }.onSuccess {

            }.onFailure {
                tv.text = ""
            }
        }

    }
}
