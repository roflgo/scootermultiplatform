package com.example.multiplatofrm.electroscooterapplication.android

import android.app.Application
import koin.listModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.module

class MainApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        val androidModule = module {
            singleOf(::ScooterPresenter)
        }

        startKoin {
            androidContext(this@MainApplication)
            androidLogger()
            modules(listModule() + androidModule)
        }
    }
}