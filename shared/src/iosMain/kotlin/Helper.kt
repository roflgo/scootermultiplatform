package kotlin

import co.touchlab.kampkit.ktor.ScooterApiImpl
import koin.listModule
import models.ScooterModel
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import org.koin.core.context.startKoin

class GreetingHelper : KoinComponent {
    private val api : ScooterApiImpl by inject()
    suspend fun greet() : List<ScooterModel> = api.getJsonFromApi()
}

fun initKoinIos() {
    startKoin {
        modules(listModule())
    }
}