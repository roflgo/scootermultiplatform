package com.example.multiplatofrm.electroscooterapplication

import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.serialization.kotlinx.json.*
import models.ScooterModel

class Greeting {
    private val client = HttpClient() {
        expectSuccess = true
        install(ContentNegotiation) {
            json()
        }
        install(HttpTimeout) {
            val timeout = 30000L
            connectTimeoutMillis = timeout
            requestTimeoutMillis = timeout
            socketTimeoutMillis = timeout
        }
    }

    suspend fun greeting(): String {
        return "Hello, ${getScooter()} ${Platform().platform}!"
    }

    private suspend fun getScooter(): ScooterModel {
        val response: HttpResponse = client.get("http://10.0.2.2:8000/api/get-scooter/")
        return response.body()
    }
}