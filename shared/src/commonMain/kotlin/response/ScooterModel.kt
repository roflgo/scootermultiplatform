package models

import kotlinx.serialization.Serializable

@Serializable
data class ScooterModel(val title: String, val id: Int, val image: String)
