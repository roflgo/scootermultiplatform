package koin.modules

import co.touchlab.kampkit.ktor.ScooterApi
import co.touchlab.kampkit.ktor.ScooterApiImpl
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.module

val coreModule = module {
    singleOf(::ScooterApiImpl)
}