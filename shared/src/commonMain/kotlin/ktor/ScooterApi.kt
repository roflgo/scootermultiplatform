package co.touchlab.kampkit.ktor

import models.ScooterModel


interface ScooterApi {
    suspend fun getJsonFromApi(): List<ScooterModel>
}
